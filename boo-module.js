import foo from './foo-module.js';

export default {
    getFoo: () => {
        return foo.value;
    },
};
