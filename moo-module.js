import foo from './foo-module.js';

export default {
    updateFoo: () => {
        foo.value = "Updated foo value";
    },
}
