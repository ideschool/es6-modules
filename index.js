import foo from './foo-module.js';
import moo from './moo-module.js';
import boo from './boo-module.js';

console.log('First time foo value: ', foo.value);
moo.updateFoo();
console.log('Second time foo value, after update: ', foo.value);
foo.value = 'Yet, another foo update';
const fooValue = boo.getFoo();
console.log('Second time foo value, after update: ', fooValue);
